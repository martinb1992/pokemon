//
//  Pokemon.swift
//  pokemon
//
//  Created by Martin Balarezo on 13/6/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import Foundation


struct Pokemon: Decodable {
    var id:Int
    var name: String
    var weight: Int
    var height: Int
    var sprites: Sprite
    
}

struct Sprite: Decodable {
    var dafaultSprite: String
    
    enum CodingKeys: String, CodingKey {
        case dafaultSprite = "front_default"
    }
    
}
