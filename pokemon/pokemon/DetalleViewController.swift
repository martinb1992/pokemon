//
//  DetalleViewController.swift
//  pokemon
//
//  Created by Martin Balarezo on 20/6/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import UIKit

class DetalleViewController: UIViewController {

    
    @IBOutlet weak var lblNombre: UILabel!
    
    @IBOutlet weak var lnlPeso: UILabel!
    
    @IBOutlet var lblAltura: UIView!
    
    @IBOutlet weak var lblid: UILabel!
    
    
    
    
    
    @IBOutlet weak var pokemonImage: UIImageView!
    
    @IBOutlet weak var idLabel: UILabel!
    
    @IBOutlet weak var nombreLabel: UILabel!
    
    var pokemon:Pokemon!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func fillData(){
        
        lblid.text = "\(pokemon.id!)"
        lblNombre.text = "\(pokemon.name ?? "")"
        
        if pokemon.imagen == nil {
            
            let bm = SBackendManager()
            
            bm.getImage((pokemon?.id)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.pokemonImage.image = imageR
                }
                
            })
            
        } else {
            
            pokemonImage.image = pokemon.imagen
            
        }
        
    }

}
