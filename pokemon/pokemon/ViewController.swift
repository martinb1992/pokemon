//
//  ViewController.swift
//  pokemon
//
//  Created by Martin Balarezo on 13/6/18.
//  Copyright © 2018 Martin Balarezo. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    @IBOutlet weak var pokemonTableview: UITableView!
    
    
    var pokemon: [Pokemon] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let network = Network()
        network.getAllPokemon{(pokemonArray) in
            self.pokemon = pokemonArray
            self.pokemonTableview.reloadData()
            
        }
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = UITableViewCell()
            cell.textLabel?.text = pokemon[indexPath.row].name
            return cell
        }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

